# MongoKanbanSeriousGames

## Contenido

* [Introduccion](#introducci%C3%B3n).
  * [Objetivos](#objetivos).
* [Justificacion](#justificaci%C3%B3n).
  * [Modelo usado](#modelo-usado).
  * [Selección de tecnología](#selecci%C3%B3n-de-tecnolog%C3%ADa).
  * [Instalación](#instalaci%C3%B3n).
* [Modelado](#modelado).
* [Implementación](#implementaci%C3%B3n).
  * [Schema / Técnica de guardado](#schema-t%C3%A9cnica-de-guardado).
  * [Queries](#queries).
* [Conclusiones](#conclusiones).
  * [Tamaño de datos](#tama%C3%B1o-de-datos).
  * [Lenguaje de programación](#lenguaje-de-programaci%C3%B3n).
* [Comparativa de lo aprendido](#comparativa-de-lo-aprendido).

## Introducción

De acuerdo con cifras del Censo Económico 2014 del INEGI, las micro, pequeñas y medianas empresas en México representan el 99.8% del total de empresas, proporcionan el 74% del total de puestos de trabajo, sin embargo, solamente aportan el 36% de la producción del país.

Uno de los principales determinantes del crecimiento económico es la productividad, entendida como la relación entre la producción total de bienes y servicios, tanto de una empresa como de un país, y las cantidades totales de factores de la producción utilizados para ello (INEGI, 1996); es decir, cuando se habla de un incremento en la productividad, se hace referencia a un aumento en la producción valiéndose de la misma cantidad de factores de producción.

Un aumento en la productividad de un país es fundamental para mantener un crecimiento económico sostenido. Los determinantes de la productividad de una economía residen en su gente: en la capacidad de sus estudiantes para adquirir nuevos conocimientos y llevarlos a la práctica; en la facilidad con la que los trabajadores puedan incorporar nuevas tecnologías e identificar oportunidades para hacer más eficientes los procesos productivos; y en la habilidad de los miembros de la comunidad científica y tecnológica de generar nuevo conocimiento e innovar.

### Producto interno bruto por hora trabajada para países seleccionados de la OCDE, 2016
<img src="../resources/productividad_OCDE_2016.PNG"
     alt="Producto interno bruto por hora trabajada para países seleccionados de la OCDE, 2016"
     style="float: center; margin-right: 10px;" />

> En 2016, la productividad laboral en México, medida como el PIB por hora trabajada, fue la más baja entre los países de la OCDE: 18.8 dólares por hora, mientras que el promedio de la OCDE fue de 47.1 dólares. Los países con la mayor productividad laboral fueron Irlanda, Luxemburgo y Noruega, que producen 83.1, 81.2 y 79.1 dólares por hora, es decir, en una hora producen al menos cuatro veces lo que se produce en México. Aun los países con menor productividad laboral de la OCDE (Chile, Letonia y Polonia) producen al menos 27% más de lo que se hace por hora en México.

### Índice Global de Productividad Laboral de la Economía por sector, 2006-2016
<img src="../resources/productividad_por_sector_2006_2016.PNG"
     alt="Índice Global de Productividad Laboral de la Economía por sector, 2006-2016"
     style="float: center; margin-right: 10px;" />

> Se advierte que la productividad laboral estimada en horas trabajadas, de los sectores primario y terciario, tuvo un crecimiento promedio anual positivo en el periodo 2006-2017 y con mayor intensidad en 2012-2017 (1.3% frente a 2% en el sector primario y 1% frente a 2.1% en el terciario), en tanto que este indicador decreció para el sector secundario en ambos periodos.

El enfoque Just-in-time (JIT) para controlar los sistemas de fabricación con "Kanban" ha recibido mucha atención en la última década. La idea de Kanban se originó en supermercados estadounidenses (Ohno 1988, págs. 25 ± 27), donde los clientes obtienen: (1) lo que se necesita, (2) en el momento en que se necesita y (3) la cantidad requerida.

El concepto de sistemas "push" se ha utilizado en la industria durante mucho tiempo. En un sistema "push", los trabajos se envían a la primera etapa de fabricación y, a su vez, esta etapa empuja el trabajo en proceso a la etapa siguiente y así sucesivamente, hasta que se obtienen los productos finales.

El sistema Kanban se conoce como un sistema 'pull' en el sentido de que la producción de la etapa actual depende de la demanda de las etapas posteriores, es decir, la etapa anterior debe producir solo la cantidad exacta retirada por la etapa de fabricación posterior. De esta manera, el sistema Kanban fue creado para indicar lo que se necesita en cada etapa de producción y para permitir que varias etapas se comuniquen entre sí de manera eficiente.

Una forma de aumentar la productividad de las empresas es implementando Kanban, pero se tiene que buscar una forma adecuada para entrenar a las personas con el fin de que puedan aplicar sus conocimientos en sus trabajos. Los estudios sobre aprendizaje basado en juegos, o juegos educativos, han encontrado efectos positivos de aprendizaje en una variedad de campos.

El propósito de esta investigación es crear una biblioteca de juegos serios, actividades de gamificación o simulaciones que ayuden a enseñar Lean Kanban a las PyMEs de México para aumentar su productividad.

### Objetivos

* Investigar que juegos serios, actividades de gamificación y/o simulaciones existen para enseñar Lean Kanban.
* Analizar sus características y objetivos de aprendizaje.
* Mapear los conceptos de Lean Kanban con los elementos de los juegos serios, actividades de gamificación y/o simulaciones.
* Practicar y evaluar el aprendizaje de los juegos serios, actividades de gamificación y/o simulaciones que cubran más conceptos de Lean Kanban.

## Justificación

En este apartado se justifica el modelo que se utilizó para almacenar los datos obtenidos en la investigación. Cabe mencionar que el alcance de este trabajo de investigación es identificar que juegos serios, actividades de gamificación y/o simulaciones ayudan a enseñar la metodología de Lean Kanban de una forma más eficiente que la tradicional, así como identificar sus características y probar su eficiencia para crear la biblioteca de juegos. No se hará una implementación de una solución de software por lo que el tener toda la información en la base de datos es solo para facilitar las consultas ya que se almacenará todo en un mismo lugar.

De igual manera, si en un trabajo a futuro se desea crear una aplicación que te sugiera un determinado juego según tu experiencia en Lean Kanban, tiempo o cantidad de jugadores, se podrá utilizar y extender facilmente esta base de datos.

### Modelo usado

* **Base de datos de Documentos:** Es un tipo de base de datos no relacional que ha sido diseñada para almacenar y consultar datos como documentos de tipo JSON. Las bases de datos de documentos facilitan a los desarrolladores el almacenamiento y la consulta de datos en una base de datos mediante el mismo formato de modelo de documentos que emplean en el código de aplicación. La naturaleza flexible, semiestructurada y jerárquica de los documentos y las bases de datos de documentos permite que evolucionen según las necesidades de las aplicaciones. El modelo de documentos funciona bien con casos de uso como catálogos, perfiles de usuario y sistemas de administración de contenido en los que cada documento es único y evoluciona con el tiempo.

Debido a que se intenta crear un catálogo de juegos serios, actividades de gamificación y/o simulaciones que ayuden a enseñar Lean Kanban, el uso de este modelo es adecuado porque nos permite ser flexibles con la estructura, así como evolucionar conforme vaya creciendo la biblioteca.

Las bases de datos de documentos son eficientes y efectivas para almacenar información de catálogo. En este mismo contexto, los diferentes juegos serios, actividades de gamificación y/o simulaciones generalmente tienen diferentes números de atributos. La administración de estos atributos en bases de datos relacionales no es eficiente y afecta al rendimiento de lectura. Al utilizar una base de datos de documentos, los atributos de cada documento se pueden describir en un solo documento para que la administración sea fácil y la velocidad de lectura sea más rápida. Cambiar los atributos de un documento no afectará a otros.

Así mismo el almacenar la información en una estructura en documentos con todos sus atributos es una forma más natural que la de tablas relacionadas.

### Selección de tecnología

**MongoDB** es una base de datos no SQL de código abierto que ofrece soporte para sistemas de almacenamiento de estilo JSON orientados a documentos. Es compatible con un modelo de datos flexible que le permite almacenar datos de cualquier estructura, y proporciona un amplio conjunto de características, que incluyen compatibilidad total con la indexación, la fragmentación y la replicación.

Las razones para utilizar MongoDB como motor de base de datos no relacional son:

1. **Flexibilidad:** MongoDB es Schema Less lo que hace que la base de datos crezca con la aplicación sin tener que ejecutar scripts que crean campos con valores por defecto cada vez que se requiera agregar documentos. En MongoDB es normal que los documentos dentro de una colección no tengan exactamente los mismos campos.
2. **Tolerancia a particiones:** Si se tienen usuarios en muchas partes del mundo y se quiere que las aplicaciones se encuentren distribuidas regionalmente, MongoDB permite tener cluster distribuidos. Esto mejora la velocidad de consulta al disminuir la latencia que existe entre el cluster de base de datos y el servicio que ejecuta la query. Además de la ventaja adicional de que si una región no se encuentra disponible, las otras pueden mantener la aplicación disponible.
3. **Transacciones:** La versión 4.0 MongoDB nos trae transacciones ACID (Atomicity, Consistency, Isolation, Durability) entre múltiples documentos.
4. **Poderoso lenguaje de consultas:** En MongoDB existen múltiples operadores que nos permiten crear consultas poderosas con poco código, además que contamos con las Agregaciones que son un mecanismo que nos permite realizar operaciones entre múltiples colecciones.
5. **Es de código abierto:** No tienes que pagar licencias para usar MongoDB dentro de tu proyecto de forma comercial.
6. **Gran comunidad:** MongoDB es el motor de base de datos no SQL basado en documentos más popular por lo que existen una gran comunidad que lo respalda.
7. **Soporte para varios lenguajes de programación** Según la documentación oficial de MongoDB, este sistema tiene drivers oficiales para los siguientes lenguajes de programación, por lo que si en un futuro se quiere crear una aplicación que implemente esta biblioteca se podrá utilizar cualquiera de los siguentes:
   * C
   * C++
   * C# / .NET
   * Java
   * JavaScript
   * Node.js
   * Perl
   * PHP
   * Python
   * Ruby
   * Scala

### Instalación

#### Version para Windows

##### Requisitos previos

 * Soporta arquitecturas x86 y x64.
 * Windows 7/Server 2008 R2.
 * Windows 8/2012 R2 y posteriores.

##### MongoDB Edicion de la comunidad.

Sigue los pasos que menciona su página oficial [Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/).

#### Versión para Linux

Sigue los pasos que menciona su página oficial [Linux](https://docs.mongodb.com/manual/administration/install-on-linux/).

#### Versión para Mac

Sigue los pasos que menciona su página oficial [Mac](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/).

#### Página oficial de MongoDB

Web oficial de [MongoDB](https://www.mongodb.com/es).

Documentación oficial [Documentación](https://docs.mongodb.com/).

### Base de datos Kanban

La base de datos esta en la carpeta data de este repositorio.

Para instalar la base de datos utiliza el comando ```mongorestore```

## Modelado

1.  **Entidades**
   *  Juegos serios. - En esta entidad se estarán guardando las características de los juegos serios.
     
| Campo | Tipo | Descripción | 
| ------ | ------ | ------ |
| nombre | String | Define el nombre del juego |
| autores | Array | Arreglo de autores | 
| objetivo | Text | Describe el objetivo del juego | 
| descripcion | Text | Descripción general del juego | 
| tiempo | int | Define el tiempo de duración del juego en minutos | 
| tipo_actividad | enum | Define que tipo de actividad es: juego serio, actividad de gamificación o simulación | 
| licencia | Array | Arreglo de licencias del juego | 
| documentacion | String | La liga a la documentación | 
| complejidad | enum | Define la complejidad del juego: baja, media o alta | 
| dificultad_implementacion | enum | Define que tan difícil es jugarlo: baja, media o alta | 
| jugadores | Object | Esta dividido en mínimo y máximo de jugadores | 
| manuales | String | La liga a los manuales, de existir | 
| videos | String | La liga a videos, de existir | 
| idioma | Array | Define los idiomas en los que está disponible el juego | 
| practicas_lean_Kanban | Object | Se divide en las 6 prácticas de Lean Kanban, y se define el elemento del juego que implementa tal práctica | 
| principios_lean_kanban | Object | Se divide en los 4 principios de Lean Kanban, y se define el elemento del juego que implementa tal principio | 
| metricas_lean_kanban | Object | Se divide en las 3 métricas de Lean Kanban, y se define el elemento del juego que implementa tal métrica | 

   *  Encuestas. - Representa las encuestas realizadas.
   
| Campo | Tipo | Descripción | 
| ------ | ------ | ------ |
| nombre | String | Define la relación entre los juegos serios y las encuestas |
| tipo | enum | Define que tipo de encuesta es: previa o posterior |
| fecha_aplicacion | Date | Define la fecha en que se aplica la encuesta |

   *  Respuestas. - Representan las respuestas a las encuestas.
   
| Campo | Tipo | Descripción | 
| ------ | ------ | ------ |
| id_pregunta | String | Define un identificador de la pregunta |
| pregunta | String | Define la pregunta de la encuesta |
| respuestas | Array | Es la colección de las respuestas a dicha pregunta |  

2.  **Relaciones**
   *  Existe una relación "One-to-Many" de juegos serios a encuestas. - Un juego serio puede tener de cero a muchas encuestas según el tipo de encuesta o la población a la que será dirigida, ejemplo, una encuesta previa sirve para definir el nivel de conocimiento de los participantes y una encuesta posterior para determinar que tanto se mejora el conocimiento de los conceptos de Lean Kanban.
   *  Existe una relación "One-to-Many" de encuestas a respuestas. - Una encuesta puede tener de cero a muchas respuestas para cada pregunta ya que algunas preguntas no son aplicadas a todas los participantes.
    

<img src="../resources/MongoDB_Detailed_Model.png"
     alt="Modelo de la base de datos con campos."
     style="float: center; margin-right: 10px;" />
     
3.  **Patrón** 
   *  **One-to-Many Relation with Embedded Documents**: En este modelo se estará consultando las respuestas más que las preguntas de las encuestas por tal motivo el utilizar este patrón nos da la ventaja de recuperar la información desde un mismo documento, a diferencia que, con documentos referenciados, la aplicación necesita emitir múltiples consultas para resolver la referencia.
      

4.  **Operaciones / Consultas**
   *  **Consultar juegos serios según características**. - Se tendrá una colección de juegos y se definen varias características de importancia, como el nombre, autores, licencias, tiempo de juego, número mínimo y máximo de jugadores, objetivo e idioma. Pero aún más importante son las prácticas, principios y métricas de Lean Kanban que implementa, por lo tanto, consultas comunes serán encontrar juegos que incluyan una determinada o determinadas prácticas, principios y/o métricas.
   *  **Consultar respuestas**. - La mayoría de las veces se estarán consultando las respuestas de una o varias preguntas con la finalidad de realizar un análisis estadístico.
   *  **Comparación entre tipos de encuestas**. - Esta operación es un conjunto de consultas a respuestas de encuestas previas y posteriores con el fin de comparar el conocimiento previo de los conceptos de Lean Kanban y el posterior para identificar si mejoró o no.


## Implementación

Se definen dos colecciones, una de juegos serios y otra de encuestas con respuestas. Es mejor trabajarlo de esta forma porque por una parte se tienen los juegos serios y sus diferentes características y por otro lado se tienen las encuestas con respuestas. Los juegos estarán relacionados a las encuestas y estas últimas servirán para evaluar la eficacia al momento de enseñar la metodología Lean Kanban. De esta forma se pueden realizar operaciones en una colección sin afectar a la otra.

La colección de juegos serios servirá para identificar los elementos que implementan las principales prácticas, principios y métricas de Lean Kanban, así cuando se quiera enseñar algún concepto se podrá consultar que juegos implementan ese concepto, así como demás características de interés como el nombre, autores, número de jugadores que acepta, documentación, licencia, el idioma entre otras.

La colección de encuestas con respuestas servirá para recabar información para evaluar que tan eficiente es el juego serio al momento de enseñar dichos conceptos de Lean Kanban por lo que las operaciones más comunes serán las de extracción de respuestas para realizar un análisis estadístico.

El siguiente es un ejemplo de la implementación de la colección de juegos serios:

```json
{
  "_id": "objectID",
  "nombre": "GetKanban 5.0",
  "autores": [
    "Russell Healy"
  ],
  "objetivo": "Texto del objetivo",
  "descripcion": "Texto de la descripcion",
  "tiempo": 150,
  "tipo_actividad": "juego",
  "licencia": [
    "Creative Commons",
    "BY",
    "NC",
    "SA"
  ],
  "documentacion": "link",
  "complejidad": "baja",
  "dificultad_implementacion": "baja",
  "jugadores": {
    "min": 4,
    "max": 6
  },
  "manuales": [
    "link"
  ],
  "video": [
    "link"
  ],
  "idioma": [
    "ingles"
  ],
  "practicas_lean_kanban": {
    "visualizar_flujo_trabajo": "tablero",
    "limitar_trabajo_proceso": "tablero",
    "manejar_flujo": "",
    "politicas_explicitas": "reglas del juego",
    "bucles_retroalimentacion": "dinámica de incio de turno",
    "mejorar_colaborativamente": "dinámica de elegir en conjunto"
  },
  "principios_lean_kanban": {
    "empezar_donde_estamos": "primera iteración",
    "perseguir_cambio_evolutivo_incremental": "iteración 1 y 2",
    "respetar_proceso_roles_responsabilidades_actual": "",
    "fomentar_actos_liderazgo_todos_niveles": ""
  },
  "metricas_lean_kanban": {
    "lead_time": "Diagrama de flujo acumuldado",
    "wip": "Diagrama de flujo acumulado",
    "throughput": ""
  }
}
```

El siguiente es un ejemplo de como quedaría implementado la colección de encuestas con respuestas utilizando el patrón "One-to-Many Relation with Embedded Documents":

```json
{
  "nombre": "Featureban V3",
  "tipo": "previa",
  "fecha_aplicacion": "2019-09-10",
  "preguntas": [
    {
      "id_pregunta": "P01",
      "pregunta": "Edad",
      "respuestas": [
        21,
        27,
        35,
        40,
        23
      ]
    },
    {
      "id_pregunta": "P02",
      "pregunta": "Genero",
      "respuestas": [
        "Hombre",
        "Mujer",
        "Hombre",
        "Hombre",
        "Mujer"
      ]
    }
  ]
}
```

Como se puede apreciar en el ejemplo, se tiene un arreglo de subdocumentos de preguntas y dentro de estas se define un arreglo de respuestas. De esta forma se puede tener una cantidad indefinida de preguntas con sus respectivas respuestas. En MongoDB existen operaciones de agregación que permiten descomponer los arreglos de subdocumentos y trabajar individualmente con ellos, esto permitirá acceder a las respuestas ya sea con la pregunta o con su identificador. 

### Schema / Técnica de guardado

En este apartado se define un schema simple de la colección de juegos serios, actividades de gamificación y simulaciones con el fin de tener una estructura similar entre todos los documentos. Su validación será ```warm``` donde MongoDB registra cualquier violación pero permite que la inserción o actualización continúe.

```json
{
  "bsonType": "object",
  "required": [
    "nombre",
    "objetivo",
    "tipo_actividad",
    "licencia",
    "documentacion",
    "complejidad",
    "dificultad_implementacion",
    "jugadores",
    "idioma",
    "practicas_lean_kanban",
    "principios_lean_kanban",
    "metricas_lean_kanban"
  ],
  "properties": {
    "nombre": {
      "bsonType": "string",
      "description": "Debe ser un String del nombre del juego y es requerido"
    },
    "autores": {
      "bsonType": "array",
      "additionalProperties": false,
      "items": {
        "bsonType": "string",
        "description": "Nombre de cada autor"
      },
      "description": "Debe ser una colección de autores si existe"
    },
    "objetivo": {
      "bsonType": "string",
      "description": "Debe ser un String donde describe el objetivo del juego y es requerido"
    },
    "descripcion": {
      "bsonType": "string",
      "description": "Debe ser un String de la descripcion del juego si existe"
    },
    "tiempo": {
      "bsonType": "int",
      "minimum": 10,
      "description": "Debe ser un entero que representa los minutos de la duracion del juego si existe"
    },
    "tipo_actividad": {
      "enum": [
        "juego",
        "gamificacion",
        "simulacion"
      ],
      "description": "Puede ser uno de los valores del enum y es requerido"
    },
    "licencia": {
      "bsonType": "array",
      "additionalProperties": false,
      "items": {
        "bsonType": "string",
        "description": "Licencia"
      },
      "description": "Debe ser una colección de licencias del juego y es requerido"
    },
    "documentacion": {
      "bsonType": "string",
      "description": "Debe ser una liga a la documentacion del juego y es requerido"
    },
    "complejidad": {
      "enum": [
        "baja",
        "media",
        "alta"
      ],
      "description": "Puede ser uno de los valores del enum y es requerido"
    },
    "dificultad_implementacion": {
      "enum": [
        "baja",
        "media",
        "alta"
      ],
      "description": "Puede ser uno de los valores del enum y es requerido"
    },
    "jugadores": {
      "bsonType": "object",
      "properties": {
        "min": {
          "bsonType": "int",
          "description": "Debe ser el numero minimo de jugadores si existe"
        },
        "max": {
          "bsonType": "int",
          "description": "Debe ser el numero maximo de jugadores si existe"
        }
      },
      "description": "Numero minimo y maximo de jugadores si existe"
    },
    "manuales": {
      "bsonType": "array",
      "additionalProperties": false,
      "items": {
        "bsonType": "string",
        "description": "Debe ser una liga al manual del juego si existe"
      },
      "description": "Es una coleccion de las ligas a los manuales del juego si existen"
    },
    "video": {
      "bsonType": "array",
      "additionalProperties": false,
      "items": {
        "bsonType": "string",
        "description": "Debe ser una liga al video del juego si existe"
      },
      "description": "Es una coleccion de las ligas a los videos del juego si existen"
    },
    "idioma": {
      "bsonType": "array",
      "additionalProperties": false,
      "items": {
        "bsonType": "string",
        "description": "Idiomas del juego"
      },
      "description": "Es una coleccion de los idiomas del juego y es requerido"
    },
    "practicas_lean_kanban": {
      "bsonType": "object",
      "properties": {
        "visualizar_flujo_trabajo": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea la practica de lean kanban si existe"
        },
        "limitar_trabajo_proceso": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea la practica de lean kanban si existe"
        },
        "manejar_flujo": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea la practica de lean kanban si existe"
        },
        "politicas_explicitas": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea la practica de lean kanban si existe"
        },
        "bucles_retroalimentacion": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea la practica de lean kanban si existe"
        },
        "mejorar_colaborativamente": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea la practica de lean kanban si existe"
        }
      },
      "description": "Elementos del juego que mapean a cada practica de lean kanban"
    },
    "principios_lean_kanban": {
      "bsonType": "object",
      "properties": {
        "empezar_donde_estamos": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea al principio de lean kanban si existe"
        },
        "perseguir_cambio_evolutivo_incremental": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea al principio de lean kanban si existe"
        },
        "respetar_proceso_roles_responsabilidades_actual": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea al principio de lean kanban si existe"
        },
        "fomentar_actos_liderazgo_todos_niveles": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea al principio de lean kanban si existe"
        }
      },
      "description": "Elementos del juego que mapean a cada principio de lean kanban"
    },
    "metricas_lean_kanban": {
      "bsonType": "object",
      "properties": {
        "lead_time": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea o usa la metrica de lean kanban si existe"
        },
        "wip": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea o usa la metrica de lean kanban si existe"
        },
        "throughput": {
          "bsonType": "string",
          "description": "Elemento del juego que mapea o usa la metrica de lean kanban si existe"
        }
      },
      "description": "Elementos del juego que mapean a cada metrica de lean kanban"
    }
  }
}
```

En seguida se define un schema para la colección de encuestas con respuestas donde la validación será ```moderate``` donde MongoDB aplica reglas de validación para operaciones de inserción y actualización a documentos existentes que ya cumplen con los criterios de validación. Las actualizaciones de los documentos existentes que no cumplen con los criterios de validación no se verifican su validez. La finalidad de tener un schema es para que se respete la estructura definida y sea más fácil realizar consultas.


```json
{
  "bsonType": "object",
  "required": [
    "nombre",
    "tipo",
    "preguntas"
  ],
  "properties": {
    "nombre": {
      "bsonType": "string",
      "description": "Debe ser un String del nombre del juego y es requerido. Determina la relacion con los juegos serios"
    },
    "tipo": {
      "enum": [
        "previa",
        "posterior"
      ],
      "description": "Puede ser uno de los valores del enum y es requerido"
    },
    "preguntas": {
      "bsonType": "array",
      "additionalProperties": false,
      "items": {
        "bsonType": "object",
        "required": [
          "id_pregunta",
          "pregunta",
          "respuestas"
        ],
        "properties": {
          "id_pregunta": {
            "bsonType": "string",
            "description": "Debe ser un String que define el identificador de la pregunta y es requerido."
          },
          "pregunta": {
            "bsonType": "string",
            "description": "Debe ser un String que define la pregunta y es requerido."
          },
          "respuestas": {
            "bsonType": "array",
            "additionalProperties": false,
            "items": {
              "description": "Repuesta"
            },
            "description": "Colección de respuestas separada por comas"
          }
        }
      },
      "description": "Colección de documentos de preguntas"
    }
  }
}
```

### Queries

#### Queries a colección seriousGames

Las consultas a esta colección son de los más sencillas ya que generalmente se buscará según alguna característica de iteres, ejemplo:

```db.seriousGames.find({"tipo_actividad" : "juego"}).pretty()```

```json
{
        "_id" : "ObjectId('5dc07762a127d7b0788879d4')",
        "nombre" : "GetKanban 5.0",
        "autores" : [
                "Russell Healy"
        ],
        "objetivo" : "El juego de mesa GetKanban es un juego físico diseñado para enseñar los conceptos y la mecánica de Kanban para el desarrollo de software en un entorno de clase o taller. Es práctico, entretenido y divertido.",
        "descripcion" : "Cada equipo es una empresa de software. Su empresa hace una aplicación web con un modelo de ingresos basado en suscripciones. Cuantos más suscriptores atraigas, más dinero ganarás. Tu objetivo es maximizar el beneficio.",
        "tiempo" : 150,
        "tipo_actividad" : "juego",
        "licencia" : [
                "Propietaria"
        ],
        "documentacion" : "https://getkanban.com/",
        "complejidad" : "media",
        "dificultad_implementacion" : "media",
        "jugadores" : {
                "min" : 4,
                "max" : 6
        },
        "manuales" : [
                "http://bit.ly/v5-1_0_1"
        ],
        "video" : [
                "https://www.youtube.com/results?search_query=getkanban"
        ],
        "idioma" : [
                "ingles"
        ],
        "practicas_lean_kanban" : {
                "visualizar_flujo_trabajo" : "tablero",
                "limitar_trabajo_proceso" : "tablero",
                "manejar_flujo" : "",
                "politicas_explicitas" : "reglas del juego",
                "bucles_retroalimentacion" : "juntas standup",
                "mejorar_colaborativamente" : ""
        },
        "principios_lean_kanban" : {
                "empezar_donde_estamos" : "",
                "perseguir_cambio_evolutivo_incremental" : "",
                "respetar_proceso_roles_responsabilidades_actual" : "",
                "fomentar_actos_liderazgo_todos_niveles" : ""
        },
        "metricas_lean_kanban" : {
                "lead_time" : "Cuadro de ejecución",
                "wip" : "Diagrama de flujo acumulado",
                "throughput" : "Cuadro de distribucion del tiempo de ciclo"
        }
}
{
        "_id" : "ObjectId('5dc1ec375613f291bba7de5c')",
        "nombre" : "GetKanban 2.0",
        "autores" : [
                "Russell Healy"
        ],
        "objetivo" : "El juego de mesa GetKanban es un juego físico diseñado para enseñar los conceptos y la mecánica de Kanban para el desarrollo de software en un entorno de clase o taller. Es práctico, entretenido y divertido.",
        "descripcion" : "Cada equipo es una empresa de software. Su empresa hace una aplicación web con un modelo de ingresos basado en suscripciones. Cuantos más suscriptores atraigas, más dinero ganarás. Tu objetivo es maximizar ",
        "tiempo" : 150,
        "tipo_actividad" : "juego",
        "licencia" : [
                "Freeware"
        ],
        "documentacion" : "https://getkanban.com/pages/older-versions",
        "complejidad" : "media",
        "dificultad_implementacion" : "media",
        "jugadores" : {
                "min" : 4,
                "max" : 6
        },
        "manuales" : [
                "http://bit.ly/v2-print"
        ],
        "video" : [
                "https://www.youtube.com/results?search_query=getkanban"
        ],
        "idioma" : [
                "ingles"
        ],
        "practicas_lean_kanban" : {
                "visualizar_flujo_trabajo" : "tablero",
                "limitar_trabajo_proceso" : "tablero",
                "politicas_explicitas" : "reglas del juego",
                "bucles_retroalimentacion" : "juntas standup"
        },
        "metricas_lean_kanban" : {
                "lead_time" : "Cuadro de ejecución",
                "wip" : "Diagrama de flujo acumulado",
                "throughput" : "Cuadro de distribución del tiempo de ciclo"
        }
}
```
Algunas consultas importantes para esta colección por lo general será buscar juegos que contengan la mayor cantidad de prácticas, principios y métricas principales de Lean Kanban, para ello utilizamos el comando ```$exists``` que nos regresará los documentos que tengan dicho campo, ejemplo:

```
 db.seriousGames.find({ practicas_lean_kanban : { $exists : true },
			principios_lean_kanban : { $exists : true },
			metricas_lean_kanban : { $exists : true }
})
```

Uno de los documentos de salida es:

```json
{
        "_id" : "ObjectId('5dc1ec375613f291bba7de65')",
        "nombre" : "Featureban 3.0",
        "autores" : [
                "Mike Burrows"
        ],
        "objetivo" : "Demostrar como funciona un tablero Kanban y como puede ayudar a mejorar el flujo de trabajo aplicando practicas, principios y artefactos de Lean Kanban.",
        "descripcion" : "Featureban es un juego de simulación kanban simple, divertido y altamente personalizable. Desde su creación en 2014, ha sido utilizado por entrenadores y entrenadores en eventos relacionados con Lean, Agile y Kanban en todo el mundo. El juego comienza con la gestión visual (iteración 1); después ponemos capas en cosas como límites de WIP (iteración 2) y métricas (iteración 3). Este enfoque incremental es en gran medida por diseño, y eres libre de introducir tus propios elementos de una manera similar.",
        "tiempo" : 120,
        "tipo_actividad" : "simulacion",
        "licencia" : [
                "Creative Commons",
                "BY",
                "SA"
        ],
        "documentacion" : "https://www.agendashift.com/featureban",
        "complejidad" : "baja",
        "dificultad_implementacion" : "baja",
        "jugadores" : {
                "min" : 3,
                "max" : 5
        },
        "manuales" : [
                "https://www.dropbox.com/s/nj5z450udyb6b02/featureban-slides-3.0.pdf?dl=0"
        ],
        "video" : [
                "https://www.youtube.com/watch?v=CalzTe-gRZ8"
        ],
        "idioma" : [
                "ingles"
        ],
        "practicas_lean_kanban" : {
                "visualizar_flujo_trabajo" : "El tablero",
                "limitar_trabajo_proceso" : "Límites en dos columnas",
                "politicas_explicitas" : "Las reglas del juego",
                "bucles_retroalimentacion" : "Juntas diarias por ronda"
        },
        "principios_lean_kanban" : {
                "empezar_donde_estamos" : "Primera iteracion del juego"
        },
        "metricas_lean_kanban" : {
                "lead_time" : "Se registra el día en que entra una tarea y el día en que sale para calcular y gráficar el tiempo de ciclo",
                "wip" : "Se captura el número de tareas por columna y se gráfica un diagrama de flujo acumulado",
                "throughput" : "Se calcula el desempeño"
        }
}
```

Como se puede apreciar este juego contiene cuatro de las seis prácticas, uno de los cuatro principios y las tres métricas principales de Kanban

#### Queries a colección encRes

Las consultas a esta colección son generalmente para obtener el arreglo de respuestas de cada pregunta, un ejemplo:

```
db.encRes.aggregate ([
    {"$match": { "nombre" : "Featureban V3", "tipo" : "previa", "fecha_aplicacion" : "2019-11-01"}}, 
    {"$unwind": { "path": "$preguntas"}}, 
    {"$match": { "preguntas.pregunta" : "Edad"}}  
])
```

En esta consulta utilizamos tuberías de etapas de agregación; ```$match``` se utiliza para filtrar el documento; ```$unwind``` descompone un campo de arreglo a partir de los documentos de entrada para generar un documento para cada elemento, en el ejemplo se descompone por el campo "preguntas" y por último utilizamos otro ```$match``` para filtrar por pregunta los subdocumentos. La salida generada es:

```json
{
        "_id" : "ObjectId('5dc32af59a56081c1671c800')",
        "nombre" : "Featureban V3",
        "tipo" : "previa",
        "fecha_aplicacion" : "2019-11-01",
        "preguntas" : {
                "id_pregunta" : "P01",
                "pregunta" : "Edad",
                "respuestas" : [
                        "Más de 35 años",
                        "18 - 23 años",
                        "18 - 23 años",
                        "Más de 35 años",
                        "Más de 35 años",
                        "Más de 35 años",
                        "18 - 23 años",
                        "Más de 35 años",
                        "Menos de 18 años",
                        "30 - 35 años",
                        "30 - 35 años",
                        "Más de 35 años"
                ]
        }
}
```

Si quisieramos dejar solo el campo de respuestas, a la consulta podemos agregar otra etapa de agregación ```$project``` que sirve para definir que campos queremos que pasen a la siguiente etapa de la tubería.

Así mismo podemos hacer operaciones como obtener el promedio de una pregunta o contar la cantidad de veces que se repite una respuesta, algunos ejemplos son:

```
db.encRes.aggregate([
    {"$match": { "nombre" : "Featureban V3", "tipo" : "previa", "fecha_aplicacion" : "2019-11-01"}}, 
    {"$unwind": { path: "$preguntas"}}, 
    {"$match": { "preguntas.pregunta" : "Visualizar el trabajo"}}, 
    {"$project": { promedio : { $avg : "$preguntas.respuestas" }, "_id" : 0}}
])
```

Esta consulta obtiene el arreglo de respuestas para la pregunta de "Visualizar el trabajo" y posteriormente calcula el promedio además de quitar el campo "_id", la salida generada es:

```
{ "promedio" : 1.5 }
```

Otro ejemplo:

```
db.encRes.aggregate([
    {"$match": { "nombre" : "Featureban V3", "tipo" : "previa", "fecha_aplicacion" : "2019-11-01"}}, 
    {"$unwind": { path: "$preguntas"}}, 
    {"$match": { "preguntas.pregunta" : "Genero"}}, 
    {"$unwind": { path: "$preguntas.respuestas"}}, 
    {"$match": { "preguntas.respuestas" : "Femenino"}}, 
    {"$count": 'respuestas'}
])
```

Al igual que en la consulta anterior, primero se obtiene el arreglo de respuestas y posteriormente se separá en "preguntas.respuestas" para poder filtrar la respuesta "Femenino" y por último utilizamos el comando ```$count``` lo que genera la salida:

```
{ "respuestas" : 8 }
```

Sin embargo, si se quiere hacer un análisis de los datos es mejor utilizar una herramienta más robusta que facilite el proceso, para ello se utilizó una Notebook de Jupiter con Python 3.

 * Primero nos conectamos a la base de datos.
 * Definimos las variables que servirán de parámetros para automatizar la extracción de los datos de la base de datos.
 * Con ayuda del paquete Pandas creamos dos dataframes uno con los datos de la encuesta previa y otro con los datos de la encuesta posterior con la finalidad de analizarlos y comparar los resultados.
 * Hacemos una conversión de los tipos de datos para facilitar su análisis.
 * Y por último analizamos y visualizamos con ayuda del paquete Matplotlib.
  

El Notebook de Jupiter esta en la carpeta de data de este repositorio.

## Conclusiones

En resumen el utilizar una base de datos con un modelado por documentos me ayudó a centralizar toda la información que se genera de mi investigación y el utilizar las Notebooks de Jupiter con Python 3 y sus librerías me facilitó el análisis y visualización, además que con el script generado podré análizar las encuests posteriores con solo cambiar los parametros.

En cuanto a los resultados, tenemos que en un principio los participantes tenían nula experiencia con Kanban y se puede obervar ya que los promedios de conocimiento de los conceptos de Lean Kanban son muy bajos, una vez que se aplicó el taller con juegos serios, los promedios mejoraron considerablemente. Además se les hicieron preguntas de satisfacción que arrojan resultados favorables al utilizar juegos serios como herramienta de aprendizaje.

Aquí las gráficas:

<img src="../resources/promedio_conceptos_previa.PNG"
     alt="Promedios de conocimiento de conceptos Kanban (Encuesta previa)"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/promedio_conceptos_posterior.PNG"
     alt="Promedios de conocimiento de conceptos Kanban (Encuesta posterior)"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/experiencia_pie.PNG"
     alt="Experiencia con Kanban, donde 1 es nada de experiencia y 5 es que la persona implementa Kanban"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/barcos_divercion_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/featureban_divercion_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/reforzar_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/motiva_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/taller_juegos_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/elementos_papel_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/elementos_featureban_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/tiempo_papel_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/tiempo_featureban_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/tiempo_taller.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/explicacion_pie.PNG"
     style="float: center; margin-right: 10px;" />
     
<img src="../resources/juegos_son_pie.PNG"
     style="float: center; margin-right: 10px;" />

### Tamaño de datos

En cuanto al tamaño actual de la base de datos:

 * El tamaño es de 20.5kb.
 * Tiene 2 colecciones (seriousGames y encRes).
 * La colección seriousGames contiene 11 documentos que son los 11 juegos serios que hasta ahora se han investigado.
 * La colección encRes contiene 2 documentos, uno por cada tipo de encuesta (previa y posterior) que se hizo.

Es posible que en un futuro cercano se agreguen más documentos de juegos serios, y es muy seguro que se estarán agregando encuestas según se vaya aplicando el experimento.

### Lenguaje de programación

Se utilizó Python 3 para realizar el análisis y visualización de los datos ya que existen librerías que facilitan este proceso, librerías utilizadas:

 1. Pandas
 2. Numpy
 3. Matplotlib
 4. PyMongo (Para administrar la conexión a la base de datos)

## Comparativa de lo aprendido

Antes de la materia de Bases de datos NoSQL yo solo conocía las bases de datos relacionales, había escuchado hablar sobre el concepto NoSQL e investigue un poco pero aun así no entendía a que se refería. Al llevar esta materia ahora sé que existen diferentes formas de modelar la información tanto en clave - valor, documentos, columnas, grafos, entre otras. No solo hay más formas de modelar, sino que también existen varios motores de bases de datos que ofrecen diferentes características. 

Otra cosa que aprendí fue que el modelo dependerá de las consultas que se vayan a realizar y esto es quizás lo más importante ya que según las operaciones y consultas que se harán un modelo puede ser más efectivo que otro. 

Para mi caso, el modelo basado en documentos se ajustó efectivamente porque mi información es una biblioteca de juegos serios, y para evaluar que cumplen el objetivo de enseñar Lean Kanban se utilizó la encuesta que también puedo almacenar y posteriormente analizar. 

El llevar esta materia no solo me sirvió para conocer más acerca de bases de datos NoSQL, también me ayudó a ver las ventajas de tener la información centralizada ya que antes tenía la información en una hoja de cálculo con columnas similar a como estaría en una base de datos relacional y las encuestas las tenía en Google Forms, pero ahora tengo todo en un mismo lugar, esto en un futuro me va a ahorrar mucho tiempo ya que solo voy a tener que ingresar nuevos documentos de encuestas y utilizar el Jupiter Notebook con el script para tener el análisis en cuestión de minutos.

Así mismo me agrada la flexibilidad que tiene por ejemplo las bases de datos en documentos, ya que los documentos pueden ser diferentes entre si. Así mismo las consultas son sencillas pero poderosas y fáciles de aprender y entender una vez que se conoce el lenguaje de consultas.

En general estoy muy contento de haber llevado esta materia, sé que me va ayudar mucho en el futuro ya que me llevo una visión más amplia de lo que hay en cuanto almacenamiento de datos.